Installations
=============

this test is based on : https://www.sitepoint.com/bringing-vr-to-web-google-cardboard-three-js/

you need a version of Three.js
Get this here : https://github.com/mrdoob/three.js/archive/master.zip

curl -o three.js.zip https://codeload.github.com/mrdoob/three.js/zip/master

extract files from  :
unzip three.js.zip 
rm three.js.zip

to run on mobile device 
========================

launch à simple http server:

python -m SimpleHTTPServer


Make a wifi hotspot to allow mobile device to connect to server
then open page:

http://<ip_server>:<server_port>/three.js-master/examples/webgl_effects_stereo_VR.html
